package com.packt.tfesenko.multithreading.section1.aux;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

public class SumAction extends RecursiveAction {

	private final int[] array;
	private final int start;
	private final int end;

	private int result;

	public SumAction(int[] array, int start, int end) {
		this.array = array;
		this.start = start;
		this.end = end;
	}

	public int getResult() {
		return result;
	}

	@Override
	protected void compute() {
		if (start == end) {
			result = array[start];
			return;
		} else if (start > end) {
			return;
		}
		int midpoint = start + (end - start) / 2;

		SumAction leftSum = new SumAction(array, start, midpoint);
		SumAction rightSum = new SumAction(array, midpoint + 1, end);

		leftSum.fork(); // computed asynchronously
		rightSum.compute();// computed in the current thread
		leftSum.join(); // wait when leftSum is ready

		result = leftSum.getResult() + rightSum.getResult();
	}

	public static void main(String[] args) {
		int numberOfThreads = Runtime.getRuntime().availableProcessors();
		ForkJoinPool pool = new ForkJoinPool(numberOfThreads);

		int[] inputArray = new int[] { 1, 1, 1, 1, 1 };
		SumAction task = new SumAction(inputArray, 0, inputArray.length - 1);
		pool.invoke(task);
		int result = task.getResult();
		System.out.println("Sum: " + result);
	}

}
