package com.packt.tfesenko.multithreading.section1.aux;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class SumTask extends RecursiveTask<Integer> {

	private final int[] array;
	private final int start;
	private final int end;

	public SumTask(int[] array, int start, int end) {
		this.array = array;
		this.start = start;
		this.end = end;
	}

	@Override
	protected Integer compute() {
		int result = 0;
		if (start == end) {
			result = array[start];
			return result;
		} else if (start > end) {
			return result;
		}
		int midpoint = start + (end - start) / 2;

		SumTask leftSum = new SumTask(array, start, midpoint);
		SumTask rightSum = new SumTask(array, midpoint + 1, end);

		leftSum.fork(); // computed asynchronously
	
		result = rightSum.compute() + leftSum.join();
		return result;
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public static void main(String[] args) {
		int numberOfThreads = Runtime.getRuntime().availableProcessors();
		ForkJoinPool pool = new ForkJoinPool(numberOfThreads);
		
		int[] inputArray = new int[] { 1, 1, 1, 1, 1 };

		SumTask task = new SumTask(inputArray, 0, inputArray.length - 1);
		int result = pool.invoke(task);
		System.out.println("Sum: " + result);
	}
}
